# Fuzzy_Matching_CLR

#### What is CLR (Common Language Runtime)?
SQL CLR or SQLCLR (SQL Common Language Runtime) is technology for hosting of the Microsoft .NET common language runtime engine within SQL Server. The SQLCLR allows managed code to be hosted by, and run in, the Microsoft SQL Server environment. CLR functions can be used to access native (unmanaged) code, such as code written in C or C++. In essence, you can write code written in languages like C#, build assemblies (any *.dll file) using Visual Studio and use them within SQL server as a function (similar to a Stored Procedure).

#### When was this used?
This was requested in Project Orange when the lawyers wanted to understand what we were using for the Fuzzy Matching used in the name screening process. The ones which were used in Project Orange were Jaro-Winkler and Levenshtein Ratio. 

#### Why CLR?
Speed.

#### How many CLRs do we have in the repo?
Currently, we have 4, which are:
1. Jaro
2. Jaro-Winkler
3. Levenshtein
4. Levenstein Ratio

# How to use?
1. Download the latest release from the RELEASES section or git clone the entire repo using the following command. <br>
`git clone https://gitlab.com/wenhaulim/fuzzy_matching_clr.git`
2. Open the `main.sql` script using MS SQL Server.
3. Press `Ctrl + H` and within the script, change all 4 `...........` to the directory where you have downloaded the release or clone the repo (the directory to fuzzy_matching_clr).
4. Click  `Execute` or the `F5` button to run the script.
5. Done!