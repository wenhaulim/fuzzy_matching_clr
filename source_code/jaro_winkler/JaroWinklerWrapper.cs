using System;

using System.Data;

using System.Data.SqlClient;

using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;

using JaroWinkler;

public partial class UserDefinedFunctions

{

    [Microsoft.SqlServer.Server.SqlFunction]

    public static SqlDouble JaroWinklerDistance(string s1, string s2)

    {

        JaroWinklerDistance a = new JaroWinklerDistance();

        return a.jaro_Winkler(s1, s2);

    }

};