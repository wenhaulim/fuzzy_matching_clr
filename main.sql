/*************************************
	Author: Wen Hau 
	Create Date: 6 July 2021
	Description: CLR functions
		- Jaro
		- Jaro-Winkler
		- Levenshtein
		- Levenshtein Ratio
*************************************/

------------------------------------------------------------------------------------------- Creating database for CLR function
-- 1. create DB with me as owner
create database playground;
GO
-- 2. change to that DB
use playground;

-- 3. change ower to SA
exec  sp_changedbowner 'sa';

-- 4. following principle of least priviledges
alter database playground set trustworthy ON
go

-- 5. enable CLR as assembly to be working on server
sp_configure 'show advanced options', 1;
go
reconfigure;
go

sp_configure 'clr enabled', 1;
go 
reconfigure;
go

------------------------------------------------------------------------------------------- Jaro
-- 6. Create the assembly
CREATE ASSEMBLY MYSQLCLR_Jaro
FROM '...........' + 'assemblies\Jaro_CLR.dll' --change the directory
WITH PERMISSION_SET = UNSAFE
GO
;
--7. Create the SP/Function
CREATE FUNCTION dbo.jaro(@firstWord nvarchar(max),  @secondWord nvarchar(max)) returns float
AS EXTERNAL NAME 
MYSQLCLR_Jaro.UserDefinedFunctions.StringDistance
GO
;
------------------------------------------------------------------------------------------- Jaro Winkler
-- 6. Create the assembly
CREATE ASSEMBLY MYSQLCLR_JaroWinkler
FROM  '...........' + 'assemblies\Jaro_Winkler_CLR.dll' --change the directory
WITH PERMISSION_SET = UNSAFE
GO
;
--7. Create the SP/Function
CREATE FUNCTION dbo.jaro_winkler(@firstWord nvarchar(max),  @secondWord nvarchar(max)) returns float
AS EXTERNAL NAME 
MYSQLCLR_JaroWinkler.UserDefinedFunctions.JaroWinklerDistance
GO
;
------------------------------------------------------------------------------------------- Levenshtein
-- 6. Create the assembly
CREATE ASSEMBLY MYSQLCLR_Levenshtein
FROM  '...........' + 'assemblies\Levenshtein_CLR.dll' --change the directory
WITH PERMISSION_SET = UNSAFE
GO
;
--7. Create the SP/Function
CREATE FUNCTION dbo.levenshtein(@firstWord nvarchar(max),  @secondWord nvarchar(max)) returns int
AS EXTERNAL NAME 
MYSQLCLR_Levenshtein.FuzzyStrings.LevenshteinDistance
GO
;
------------------------------------------------------------------------------------------- Levenshtein Ratio 
-- 6. Create the assembly
CREATE ASSEMBLY MYSQLCLR_Levenshtein_Ratio
FROM  '...........' + 'assemblies\Levenshtein_Ratio_CLR.dll' --change the directory
WITH PERMISSION_SET = UNSAFE
GO
;
--7. Create the SP/Function
CREATE FUNCTION dbo.levenshtein_ratio(@firstWord nvarchar(max),  @secondWord nvarchar(max)) returns float
AS EXTERNAL NAME 
MYSQLCLR_Levenshtein_Ratio.FuzzyStrings.Calculate
GO
;
------------------------------------------------------------------------------------------- QC Checks
-- 8. use the correct DB
USE [playground];

-- 9. use the Function
select dbo.jaro ('string similarity', 'string');		
select dbo.jaro_winkler('string similarity', 'string');
select dbo.levenshtein('string similarity', 'string');
select dbo.levenshtein_ratio('string similarity', 'string');